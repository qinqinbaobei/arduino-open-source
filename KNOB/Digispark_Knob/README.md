# 基于Digispark开发板的媒体控制旋钮
## 一、材料与工具
    1.Digispark kickstarter usb 开发板 TINY85
<img src="./img/Digispark%20kickstarter.jpg" width = "30%" height = "30%" alt="Digispark kickstarter" align=center />

    2.EC11编码器 型号KY-040
<img src="./img/KY-040.jpg" width = "30%" height = "30%" alt="KY-040" align=center />
    
    3.pcb飞线
    4.电烙铁
    5.3D打印机（或其他外壳方案，如微积木+金属旋钮帽）

## 二、环境配置
### 2.1、驱动安装
    驱动地址：https://github.com/digistump/DigistumpArduino/releases/download/1.6.7/Digistump.Drivers.zip
    网盘资料：https://pan.baidu.com/share/link?shareid=3673995949&uk=2302102993

### 2.2、Arduino IDE 开发板安装
    官方文档地址：https://digistump.com/wiki/digispark/tutorials/connecting
    1.在 文件->首选项 中添加开发板管理器网址(选其一即可，若因网络失败可都加上再尝试)：
        https://raw.githubusercontent.com/ArminJo/DigistumpArduino/master/package_digistump_index.json
        http://digistump.com/package_digistump_index.json
    2.在 工具->开发板->开发板管理器 中搜索 “Digistump AVR Boards” 并安装
    3.开发板选择 Digispark
### 2.3、安装依赖库
    库地址：https://github.com/adafruit/Adafruit-Trinket-USB
    1.点击“Clone or download”按钮并选择Download ZIP
    2.解压zip压缩包并复制其中名为"TrinketHidCombo"的文件夹到Arduino的库文件中
    (库文件夹默认路径为 C:\Users\(your username)\Documents\Arduino\libraries)

## 三、焊接元件引脚
    连线图
<img src="./img/Connection.jpg" width = "90%" height = "90%" alt="连线图" align=center />

## 四、编译与上传
    1.选择开发板 Digispark 
<img src="./img/board.jpg" width = "90%" height = "90%" alt="选择开发板" align=center />

    2.点击上传（编译完成后会提示插入开发板）
<img src="./img/compile.jpg" width = "90%" height = "90%" alt="编译" align=center />   
    
    3.插入开发板完成固件上传(如果之前就插着就拔下usb重新插入)
<img src="./img/upload.png" width = "50%" height = "50%" alt="编译" align=center />  

## 五、其他资料
    项目源地址：https://www.thingiverse.com/thing:2970774