#include "MediaButton.h"

int d,e;

MediaButton::MediaButton(int dPin, int ePin){
  d = dPin;
  e = ePin;
}

void MediaButton::begin(){
  pinMode(d, OUTPUT);
  digitalWrite(d, LOW);
  pinMode(e, INPUT_PULLUP);
  SurfaceDial.begin();
}

void MediaButton::refresh(){
  //判断按键
  if(!digitalRead(e)){
    int i = 0;
    //按下循环
    while(!digitalRead(e)){
      delay(10);
      i++;
      //按下超时（静音）
      if(i>50){
        //#按钮长按保持#
        SurfaceDial.press();
        i = 0;
        while(!digitalRead(e)){
          if(i>1){
            //#按钮长按保持循环#
            SurfaceDial.press();
          }
          delay(500);
          i++;
        }
        //return;        // case 1
      }
    }
    //i = 0;
    //释放循环
    while(digitalRead(e)){
      delay(10);
      i++;
      if(i<50){
        //#按钮短按#
        SurfaceDial.click();
        return;         //case 2
      }else{
        SurfaceDial.release();
        return;
      }
    }
    return;
  }
}
