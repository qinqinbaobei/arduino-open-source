

/*
  Copyright (c) 2017 wind-rider
  See the readme for credit to other people.
  Surface dial example
  
  Use an encoder and a button to create a Surface Dial-compatible device.
  See the connection diagram how to wire it up.
  Please note that:
   - I tested it using an Arduino Pro Micro; TinkerCad didn't have that in its component library
   - you obviously don't need a motor, but TinkerCad didn't have a separate encoder
  The encoder processing code is coming from https://www.allwinedesigns.com/blog/pocketnc-jog-wheel
*/
#include "HID-Project.h"
#include "Encoder.h"
#include "MediaButton.h"
static int a=4,b=2,c=3,d=A1,e=A3;
Encoder enc1(a,b,c);
MediaButton btn1(d,e);

// input pin for pushbutton
int pinButton = A3;
int pinSecondButton = A1;

bool previousButtonValue = false;
bool previousSecondButtonValue = false;

volatile int previous = 0;
volatile int counter = 0;

void setup()
{
   enc1.begin();
   btn1.begin();
   Serial.begin(9600);
   SurfaceDial.begin();
}


#define DialRotateDelta 18

int total = 0;
int threshold = 2;
void loop()
{

   int re = enc1.refresh();
    total += re;
    if(total == threshold){
      //#编码器逆时针#
      SurfaceDial.rotate(-DialRotateDelta);
      total = 0;
    }
    else if(total == -threshold){
      //#编码器顺时针#
      SurfaceDial.rotate(DialRotateDelta);
      total = 0;
    }
    btn1.refresh();
    if(re!=0){
      Serial.println(re);
    }
}
