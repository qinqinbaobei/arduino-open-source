# 基于Arduino Pro micro的SurfaceDial

## 材料

EC11编码器

Arduino Pro Micro Type-C 开发板

## 焊接引脚

A1,A3,2,3,4

![](assets/20221117_103859_68f85b3956d85b1b59ba3a74f3eda5a.jpg)

## 依赖库安装

[HID](https://github.com/NicoHood/HID)
