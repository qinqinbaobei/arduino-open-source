//时钟模式一 HH:mm:ss
void ClockMode1()
{
  showbitnumber(H,3,5,2+GlobalX,1+GlobalY,hsv2rgb(hueh, saturation, value));
  showbitnumber(M,3,5,12+GlobalX,1+GlobalY,hsv2rgb(huem, saturation, value));
  showbitnumber(S,3,5,22+GlobalX,1+GlobalY,hsv2rgb(hues, saturation, value));
  //冒号
  showColon(10+GlobalX,2+GlobalY,true,hsv2rgb(hue, saturation, value));
  showColon(20+GlobalX,2+GlobalY,true,hsv2rgb(hue, saturation, value));
  //星期
  showWeek(2+GlobalX,7+GlobalY,huew,3);
}


//时钟模式二: HH:mm
void ClockMode2()
{
  showbitnumber(H,3,5,7+GlobalX,1+GlobalY,hsv2rgb(hueh, saturation, value));
  if ( millis() - colonTime > 1000 )
  {
    colon = !colon;
    colonTime = millis();
  }
  showColon(15+GlobalX,2+GlobalY,colon,hsv2rgb(hue, saturation, value));
  showbitnumber(M,3,5,17+GlobalX,1+GlobalY,hsv2rgb(huem, saturation, value));
  //星期
  showWeek(2+GlobalX,7+GlobalY,huew,3);  
}

//时钟模式三: 图标 + HH:mm
void ClockMode3()
{
  SetEmoji(pic_x, pic_y);
  showbitnumber(H,3,5,12+GlobalX,1+GlobalY,hsv2rgb(hueh, saturation, value));
  showColon(20+GlobalX,2+GlobalY,true,hsv2rgb(hue, saturation, value));
  showbitnumber(M,3,5,22+GlobalX,1+GlobalY,hsv2rgb(huem, saturation, value));
  //星期
  showWeek(11+GlobalX,7+GlobalY,huew,2);
}

//时钟模式四: 图标 + HH:mm + 动态冒号
void ClockMode4()
{
  SetEmoji(pic_x, pic_y);
  showbitnumber(H,3,5,12+GlobalX,1+GlobalY,hsv2rgb(hueh, saturation, value));
  showbitnumber(M,3,5,22+GlobalX,1+GlobalY,hsv2rgb(huem, saturation, value));
  //星期
  showWeek(11+GlobalX,7+GlobalY,huew,2);
  if ( millis() - colonTime > 1000 )
  {
    colon = !colon;
    colonTime = millis();
  }
  showColon(20+GlobalX,2+GlobalY,colon,hsv2rgb(hue, saturation, value));
}

//显示星期
void showWeek(int x, int y, int colorhue, int len) {
  for(int i=0;i<7;i++){
    drawFastXLine(x+i*(len+1),y,len,hsv2rgb(colorhue, saturation, value-50));
  }
  int d = D;
  if(D == 0){
    d = 7;
  }
  drawFastXLine(x+(d-1)*(len+1),y,len,hsv2rgb(colorhue, saturation, value));
}

void showFlashBack(bool implement){
  if(implement){
    itvl= 16;
    timeClient.update();
    H = timeClient.getFormattedTime().substring(0, 2).toInt();
    M = timeClient.getFormattedTime().substring(3, 5).toInt();
    S = timeClient.getFormattedTime().substring(6, 8).toInt();
    falsh_time = 0;
    Serial.println(H);
    Serial.println(M);
    Serial.println(S);
    matrix.clear();
  }
  if ( millis() - lastTime > itvl )
  {
    lastTime = millis();
    int n = falsh_interval-falsh_time;
    int t = 4;
    if( n>0 && n< 10 ){
      t = 1;
      delay((10-n)*20);
    }else if(n<0){
      itvl = 990;
      S += 1;
      if(S>59){
        S = 0;
        M += 1;
        if(M>59){
          M = 0;
          H += 1;
          if(H>23){
           H = 0; 
          } 
        }
      }
      return;  
    }
    falsh_time += t;
    S -= t;
    if(S<0){
       if(t > 1){
        S = 58;
       }else{
        S = 59;
       }
       M -= 1;
       if(M<0){
        M = 59;
        H -= 1;
        if(H<0){
          H = 23;
        }  
       }   
    }
  }
  ClockMode1();
}
