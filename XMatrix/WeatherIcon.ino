
void showWeatherDay(bool implement){
    uint16_t showColor = hsv2rgb(hue, saturation, value);
    if ( millis() - lastTime > 60000*5 || implement)
    {
        weatherNow("http://api.yytianqi.com/observe?city="+CITY_CODE+"&key="+YY_KEY);
        if(!implement){
            lastTime = millis();
        }else{
            loopTime = millis();
        }
        matrix.clear();
    }
    if(millis() - loopTime > 5000 && !implement){
      if(++weatherMode>5) weatherMode=1;
      loopTime = millis();
      matrix.clear();
    }
    switch(weatherMode){
      case 1:
        if(tq_len<10){
          showStringAlways( Cnfont8x8, tq_str, tq_len, showColor);
        }else{
          showStringSlip(23+GlobalX, 0+GlobalY, Cnfont8x8, tq_str, tq_len, showColor);
        }
        matrix.fillRect(0+GlobalX,0+GlobalY,8,8,matrix.Color(0,0,0));
        if(tq_code == "00"){
          sunnyDay();
        }else if(tq_code == "01"){ cloudyDay();
        }else if(tq_code == "02"){ overcast();
        }else if(tq_code == "03"){ rain();
        }else if(tq_code == "04"){ thunderstorm();
        }else if(tq_code == "05"){ thunderstorm();
        }else if(tq_code == "06"){ sleet();
        }else if(tq_code == "07"){ rain();
        }else if(tq_code == "08"){ rain();
        }else if(tq_code == "09"){ rain();
        }else if(tq_code == "10"){ rain();
        }else if(tq_code == "11"){ rain();
        }else if(tq_code == "12"){ rain();
        }else if(tq_code == "13"){ slightSnow(); //阵雪
        }else if(tq_code == "14"){ slightSnow();
        }else if(tq_code == "15"){ slightSnow();
        }else if(tq_code == "16"){ slightSnow();
        }else if(tq_code == "17"){ slightSnow();
        }else if(tq_code == "18"){ unknownWeather(); //雾
        }else if(tq_code == "19"){ rain(); //冻雨
        }else if(tq_code == "20"){ slightSnow(); //沙尘暴
        }else if(tq_code == "21"){ rain(); 
        }else if(tq_code == "22"){ rain();
        }else if(tq_code == "23"){ rain();
        }else if(tq_code == "24"){ rain();
        }else if(tq_code == "25"){ rain();
        }else if(tq_code == "26"){ slightSnow();
        }else if(tq_code == "27"){ slightSnow();
        }else if(tq_code == "28"){ slightSnow();
        }else if(tq_code == "100"){ windy();
        }else{ unknownWeather(); }
        break;
      case 2:
        TempPattern();
        showbitnumber(temp_read.toInt(),3,5,13+GlobalX,2+GlobalY,showColor);
        cdPattern(23+GlobalX,0+GlobalY,showColor);
        break;
      case 3:
        HumPattern();
        //SetEmoji(4, 5);
        showbitnumber(hum_read.toInt(),3,5,13+GlobalX,2+GlobalY,showColor);
        pcPattern(23+GlobalX,0+GlobalY,showColor);
        break;
      case 4:
        showTime(false);
        break;
      case 5:
        showMouthDay(false);
        break;
    }
    matrix.show();
}

//温度计图案
void TempPattern(){
  drawFastYLine(1+GlobalX,4+GlobalY,3,matrix.Color(255,255,255));
  drawFastYLine(2+GlobalX,0+GlobalY,5,matrix.Color(255,255,255));
  drawFastYLine(3+GlobalX,0+GlobalY,1,matrix.Color(255,255,255));
  drawFastYLine(4+GlobalX,0+GlobalY,5,matrix.Color(255,255,255));
  drawFastYLine(2+GlobalX,5+GlobalY,2,matrix.Color(255,48,48));
  drawFastYLine(3+GlobalX,2+GlobalY,5,matrix.Color(255,48,48));
  drawFastYLine(4+GlobalX,5+GlobalY,2,matrix.Color(255,48,48));
  drawFastXLine(2+GlobalX,7+GlobalY,3,matrix.Color(255,255,255));
  drawFastYLine(5+GlobalX,4+GlobalY,3,matrix.Color(255,255,255));
}
//湿度计图案
void HumPattern(){
  drawFastYLine(1+GlobalX,4+GlobalY,3,matrix.Color(255,255,255));
  drawFastYLine(2+GlobalX,0+GlobalY,5,matrix.Color(255,255,255));
  drawFastYLine(3+GlobalX,0+GlobalY,1,matrix.Color(255,255,255));
  drawFastYLine(4+GlobalX,0+GlobalY,5,matrix.Color(255,255,255));
  drawFastYLine(2+GlobalX,5+GlobalY,2,matrix.Color(65,105,225));
  drawFastYLine(3+GlobalX,2+GlobalY,5,matrix.Color(65,105,225));
  drawFastYLine(4+GlobalX,5+GlobalY,2,matrix.Color(65,105,225));
  drawFastXLine(2+GlobalX,7+GlobalY,3,matrix.Color(255,255,255));
  drawFastYLine(5+GlobalX,4+GlobalY,3,matrix.Color(255,255,255));
}
//℃
void cdPattern(int16_t x, int16_t y, uint16_t color){
  matrix.drawPixel(x+2,y+2,color);
  drawFastYLine(x+4,y+3,y+3,color);
  matrix.drawPixel(x+5,y+2,color);
  matrix.drawPixel(x+6,y+2,color);
  matrix.drawPixel(x+5,y+6,color);
  matrix.drawPixel(x+6,y+6,color);
}
//%
void pcPattern(int16_t x, int16_t y, uint16_t color){
  matrix.drawPixel(x+3,y+2,color);
  matrix.drawPixel(x+3,y+6,color);
  matrix.drawPixel(x+3,y+5,color);
  matrix.drawPixel(x+4,y+4,color);
  matrix.drawPixel(x+5,y+3,color);
  matrix.drawPixel(x+5,y+2,color);
  matrix.drawPixel(x+5,y+6,color);

}

// 白天晴
void sunnyDay(){
    matrix.fillRect(2+GlobalX,3+GlobalY,4,2,matrix.Color(255,255,0));
    matrix.fillRect(3+GlobalX,2+GlobalY,2,4,matrix.Color(255,255,0));
    matrix.drawPixel(2+GlobalX,0+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(5+GlobalX,0+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(0+GlobalX,2+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(7+GlobalX,2+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(0+GlobalX,5+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(7+GlobalX,5+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(2+GlobalX,7+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(5+GlobalX,7+GlobalY,matrix.Color(255,255,0));
}
// 夜晚晴
void sunnyNight(){
    matrix.drawFastVLine(0+GlobalX,3+GlobalY,3,matrix.Color(190,190,190));
    matrix.drawFastVLine(1+GlobalX,3+GlobalY,3,matrix.Color(255,255,255));
    matrix.drawFastHLine(2+GlobalX,7+GlobalY,3,matrix.Color(190,190,190));
    matrix.drawPixel(5+GlobalX,0+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(7+GlobalX,2+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(4+GlobalX,4+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(6+GlobalX,6+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(2+GlobalX,1+GlobalY,matrix.Color(190,190,190));
    matrix.drawPixel(1+GlobalX,2+GlobalY,matrix.Color(190,190,190));
    matrix.drawPixel(1+GlobalX,6+GlobalY,matrix.Color(190,190,190));
    matrix.drawPixel(3+GlobalX,1+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(2+GlobalX,2+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(2+GlobalX,6+GlobalY,matrix.Color(255,255,255));
}

// 白多云
void cloudyDay(){
    matrix.drawPixel(2+GlobalX,0+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(4+GlobalX,0+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(0+GlobalX,1+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(0+GlobalX,3+GlobalY,matrix.Color(255,255,0));
    matrix.drawFastHLine(2+GlobalX,2+GlobalY,3,matrix.Color(255,255,0));
    matrix.drawPixel(5+GlobalX,2+GlobalY,matrix.Color(255,255,255));
    matrix.fillRect(2+GlobalX,3+GlobalY,5,5,matrix.Color(255,255,255));
    matrix.drawPixel(2+GlobalX,3+GlobalY,matrix.Color(255,255,0));
    matrix.drawFastHLine(4+GlobalX,3+GlobalY,3,matrix.Color(180,180,180));
    matrix.drawFastHLine(4+GlobalX,4+GlobalY,2,matrix.Color(150,150,150));
    matrix.drawPixel(4+GlobalX,5+GlobalY,matrix.Color(150,150,150));
    matrix.drawFastVLine(1+GlobalX,5+GlobalY,2,matrix.Color(180,180,180));
    matrix.drawFastVLine(7+GlobalX,4+GlobalY,3,matrix.Color(180,180,180));
}

/* 
 * 功能：夜晚多云
 */
void cloudyNight(){
    matrix.drawFastVLine(1+GlobalX,2+GlobalY,2,matrix.Color(255,255,255));
    matrix.drawFastVLine(2+GlobalX,1+GlobalY,4,matrix.Color(255,255,255));
    matrix.drawFastHLine(2+GlobalX,4+GlobalY,2,matrix.Color(255,255,255));
    matrix.drawFastHLine(3+GlobalX,5+GlobalY,4,matrix.Color(255,255,255));
    matrix.drawFastHLine(3+GlobalX,6+GlobalY,2,matrix.Color(128,128,128));
    matrix.drawPixel(0+GlobalX,0+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(3+GlobalX,0+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(6+GlobalX,1+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(1+GlobalX,4+GlobalY,matrix.Color(128,128,128));
    matrix.drawPixel(7+GlobalX,4+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(2+GlobalX,5+GlobalY,matrix.Color(128,128,128));
    matrix.drawPixel(1+GlobalX,7+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(7+GlobalX,7+GlobalY,matrix.Color(255,255,255));
}

/* 
 * 功能：阴
 */
void overcast(){
    matrix.drawFastVLine(1+GlobalX,2+GlobalY,2,matrix.Color(100,100,100));
    matrix.drawFastVLine(5+GlobalX,1+GlobalY,2,matrix.Color(100,100,100));
    matrix.drawPixel(4+GlobalX,2+GlobalY,matrix.Color(100,100,100));
    matrix.drawPixel(7+GlobalX,2+GlobalY,matrix.Color(100,100,100));
    matrix.drawFastHLine(5+GlobalX,2+GlobalY,2,matrix.Color(200,200,200));
    matrix.drawFastHLine(0+GlobalX,3+GlobalY,8,matrix.Color(190,190,190));
    matrix.drawPixel(0+GlobalX,3+GlobalY,matrix.Color(100,100,100));
    matrix.drawPixel(3+GlobalX,3+GlobalY,matrix.Color(100,100,100));
    matrix.drawFastHLine(0+GlobalX,4+GlobalY,7,matrix.Color(180,180,180));
    matrix.drawFastHLine(0+GlobalX,5+GlobalY,7,matrix.Color(150,150,150));
    matrix.drawPixel(0+GlobalX,5+GlobalY,matrix.Color(100,100,100));
    matrix.drawPixel(7+GlobalX,5+GlobalY,matrix.Color(100,100,100));
    matrix.drawFastHLine(1+GlobalX,6+GlobalY,6,matrix.Color(110,110,110));
}

/* 
 * 功能：雨
 */
void rain(){
    matrix.fillRect(1+GlobalX,1+GlobalY,2,4,matrix.Color(255,255,255));
    matrix.fillRect(2+GlobalX,3+GlobalY,5,2,matrix.Color(255,255,255));
    matrix.fillRect(4+GlobalX,1+GlobalY,4,3,matrix.Color(255,255,255));

    matrix.drawPixel(5+GlobalX,0+GlobalY,matrix.Color(150,150,150));
    matrix.drawPixel(4+GlobalX,1+GlobalY,matrix.Color(150,150,150));
    matrix.drawPixel(7+GlobalX,1+GlobalY,matrix.Color(150,150,150));
    matrix.drawPixel(0+GlobalX,2+GlobalY,matrix.Color(150,150,150));
    matrix.drawPixel(1+GlobalX,4+GlobalY,matrix.Color(150,150,150));
    matrix.drawPixel(0+GlobalX,3+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(6+GlobalX,0+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(1+GlobalX,5+GlobalY,matrix.Color(0,255,255));
    matrix.drawPixel(0+GlobalX,6+GlobalY,matrix.Color(0,255,255));
    matrix.drawPixel(4+GlobalX,5+GlobalY,matrix.Color(0,255,255));
    matrix.drawPixel(3+GlobalX,6+GlobalY,matrix.Color(0,255,255));
    matrix.drawPixel(2+GlobalX,7+GlobalY,matrix.Color(0,255,255));
    matrix.drawPixel(7+GlobalX,5+GlobalY,matrix.Color(0,255,255));
    matrix.drawPixel(6+GlobalX,6+GlobalY,matrix.Color(0,255,255));
    matrix.drawPixel(5+GlobalX,7+GlobalY,matrix.Color(0,255,255));
}

/* 
 * 功能：雨夹雪
 */
void sleet(){
    matrix.fillRect(1+GlobalX,1+GlobalY,2,4,matrix.Color(255,255,255));
    matrix.fillRect(2+GlobalX,3+GlobalY,5,2,matrix.Color(255,255,255));
    matrix.fillRect(4+GlobalX,1+GlobalY,4,3,matrix.Color(255,255,255));

    matrix.drawPixel(5+GlobalX,0+GlobalY,matrix.Color(150,150,150));
    matrix.drawPixel(4+GlobalX,1+GlobalY,matrix.Color(150,150,150));
    matrix.drawPixel(7+GlobalX,1+GlobalY,matrix.Color(150,150,150));
    matrix.drawPixel(0+GlobalX,2+GlobalY,matrix.Color(150,150,150));
    matrix.drawPixel(1+GlobalX,4+GlobalY,matrix.Color(150,150,150));
    matrix.drawPixel(0+GlobalX,3+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(6+GlobalX,0+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(1+GlobalX,5+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(0+GlobalX,6+GlobalY,matrix.Color(0,255,255));
    matrix.drawPixel(4+GlobalX,5+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(3+GlobalX,6+GlobalY,matrix.Color(0,255,255));
    matrix.drawPixel(2+GlobalX,7+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(7+GlobalX,5+GlobalY,matrix.Color(0,255,255));
    matrix.drawPixel(6+GlobalX,6+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(5+GlobalX,7+GlobalY,matrix.Color(0,255,255));
}


/* 
 * 功能：雪晴（小雪人）
 */
void snowyDay(){
    matrix.fillRect(2+GlobalX,1+GlobalY,4,3,matrix.Color(255,255,255));
    matrix.fillRect(1+GlobalX,4+GlobalY,6,4,matrix.Color(255,255,255));
    matrix.drawFastVLine(0+GlobalX,5+GlobalY,2,matrix.Color(205,133,63));
    matrix.drawFastVLine(7+GlobalX,5+GlobalY,2,matrix.Color(205,133,63));
    matrix.drawPixel(1+GlobalX,6+GlobalY,matrix.Color(205,133,63));
    matrix.drawPixel(6+GlobalX,6+GlobalY,matrix.Color(205,133,63));
    matrix.drawFastHLine(3+GlobalX,0+GlobalY,2,matrix.Color(255,0,0));
    matrix.drawFastHLine(3+GlobalX,3+GlobalY,2,matrix.Color(100,100,100));
    matrix.drawPixel(2+GlobalX,2+GlobalY,matrix.Color(0,0,255));
    matrix.drawPixel(5+GlobalX,2+GlobalY,matrix.Color(0,0,255));
}

/* 
 * 功能：小雪
 */
void slightSnow(){
    matrix.drawFastVLine(3+GlobalX,0+GlobalY,4,matrix.Color(230,255,255));
    matrix.drawFastHLine(2+GlobalX,1+GlobalY,3,matrix.Color(230,255,255));
    matrix.drawFastVLine(1+GlobalX,3+GlobalY,3,matrix.Color(230,255,255));
    matrix.drawFastHLine(0+GlobalX,4+GlobalY,4,matrix.Color(230,255,255));
    matrix.drawFastVLine(4+GlobalX,4+GlobalY,4,matrix.Color(230,255,255));
    matrix.drawFastHLine(3+GlobalX,6+GlobalY,3,matrix.Color(230,255,255));
    matrix.drawFastVLine(6+GlobalX,2+GlobalY,3,matrix.Color(230,255,255));
    matrix.drawFastHLine(4+GlobalX,3+GlobalY,4,matrix.Color(230,255,255));
}

/* 
 * 功能：雷雨
 */
void thunderstorm(){
    matrix.fillRect(2+GlobalX,3+GlobalY,2,2,matrix.Color(255,255,255));
    matrix.fillRect(4+GlobalX,2+GlobalY,2,3,matrix.Color(255,255,255));
    matrix.drawFastVLine(1+GlobalX,3+GlobalY,2,matrix.Color(100,100,100));
    matrix.drawFastVLine(6+GlobalX,2+GlobalY,4,matrix.Color(100,100,100));
    matrix.drawFastHLine(0+GlobalX,5+GlobalY,4,matrix.Color(100,100,100));
    matrix.drawFastHLine(2+GlobalX,2+GlobalY,2,matrix.Color(100,100,100));
    matrix.drawFastHLine(4+GlobalX,1+GlobalY,2,matrix.Color(100,100,100));
    matrix.drawFastHLine(5+GlobalX,5+GlobalY,3,matrix.Color(100,100,100));
    matrix.drawPixel(4+GlobalX,3+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(3+GlobalX,4+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(4+GlobalX,5+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(3+GlobalX,6+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(2+GlobalX,7+GlobalY,matrix.Color(255,255,0));
    matrix.drawPixel(1+GlobalX,6+GlobalY,matrix.Color(0,0,255));
    matrix.drawPixel(0+GlobalX,7+GlobalY,matrix.Color(0,0,255));
    matrix.drawPixel(6+GlobalX,6+GlobalY,matrix.Color(0,0,255));
    matrix.drawPixel(5+GlobalX,7+GlobalY,matrix.Color(0,0,255));
}

/* 
 * 功能：风
 */
void windy(){
    matrix.fillRect(2+GlobalX,2,3,2,matrix.Color(255,255,255));
    matrix.drawFastHLine(0+GlobalX,0+GlobalY,2,matrix.Color(255,255,255));
    matrix.drawFastHLine(1+GlobalX,1+GlobalY,2,matrix.Color(255,255,255));
    matrix.drawFastHLine(4+GlobalX,1+GlobalY,2,matrix.Color(255,255,255));
    matrix.drawFastHLine(5+GlobalX,0+GlobalY,2,matrix.Color(255,255,255));
    matrix.drawFastHLine(4+GlobalX,4+GlobalY,3,matrix.Color(255,255,255));
    matrix.drawPixel(6+GlobalX,5+GlobalY,matrix.Color(255,255,255));
    matrix.drawFastVLine(1+GlobalX,3+GlobalY,2,matrix.Color(255,255,255));
    matrix.drawFastVLine(0+GlobalX,4+GlobalY,2,matrix.Color(255,255,255));
    matrix.drawFastVLine(3+GlobalX,4+GlobalY,4,matrix.Color(130,130,130));
}
    
/* 
 * 功能：未知天气
 */
void unknownWeather(){
    matrix.drawFastHLine(4+GlobalX,5,3,matrix.Color(255,255,255));

    matrix.drawPixel(1+GlobalX,1+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(2+GlobalX,2+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(3+GlobalX,3+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(2+GlobalX,4+GlobalY,matrix.Color(255,255,255));
    matrix.drawPixel(1+GlobalX,5+GlobalY,matrix.Color(255,255,255));
}
