# 像素时钟XMatrix

## 主控

ESP8266 nodemcu v3

## 开发环境

Arduino IDE

esp8266开发板包版本 2.7.4

## 关键依赖库版本


| 库                   | 版本       | 地址                                             |
| :--------------------- | ------------ | -------------------------------------------------- |
| NTPClient            | 3.1.0      | https://github.com/arduino-libraries/NTPClient   |
| Adafruit_NeoMatrix   | 1.2.0      | https://github.com/adafruit/Adafruit_NeoMatrix   |
| Adafruit-GFX-Library | 1.11.1     | https://github.com/adafruit/Adafruit-GFX-Library |
| WiFiManager          | 0.16(推荐) | https://github.com/tzapu/WiFiManager             |

## 天气API依赖

注册账号获取key，YY天气：[丫丫天气_天气免费API (yytianqi.com)](http://www.yytianqi.com/api.html)

城市ID：[丫丫天气_天气免费API (yytianqi.com)](http://www.yytianqi.com/citys/1.html)

代码中填写位置：config.h中搜索“天气参数定义”可找到

## 安卓APP使用说明

### 首次使用

1. 在首页填入设备的内网IP(设备开机时会滚动显示)，并点击连接完成设备连接。
   1. <img src="assets/20230114_215436_e1767caae41bbac44f29b916ce090a6.jpg" alt="drawing" width="150"/>
   2. 连接成功后，连接按钮右边的icon图标会变成√

### 自定义命令按键

#### 功能说明

可通过json字符串导入一个自定义的按键组合，json中包含了按键样式与按键实际命令。

#### json示例

```json
{
   "button":
      [
         {"name":"调节亮度","class":"mui-btn mui-btn-primary","command":"d"},
         {"name":"下一模式","class":"mui-btn mui-btn-success","command":"mn"},
         {"name":"时钟风格","class":"mui-btn mui-btn-warning","command":"mt"},
         {"name":"颜色风格","class":"mui-btn mui-btn-danger","command":"mc"}
      ]
   }
```

name：按键显示的名称
class：按键的样式
command：按键发送的命令
示例对应的按键样式如下图所示：
<img src="assets/e8847079c5c0bd73a29aa57060b43b4.jpg" alt="drawing" width="150"/>

## 更新日志

### 1.0

#### 发布时间 2022/10/29

### 1.5

#### 发布时间 2022/11/08

#### 更新内容

1. 新增B站粉丝数显示模式
   1. 支持数字颜色设置
   2. 支持数字颜色风格设置
2. 新增模式切换风格选择
   1. 上下滑动、渐隐渐显
3. 优化WiFi连接显示样式

### 2.0

#### 发布时间 2022/11/19

#### 更新内容

1. bugfix
   1. 修复低亮度模式切换动画的bug
   2. 其他细节修复
2. 支持天气实况显示（温湿度、天气现象）

### 3.0

#### 发布时间 2023/01/14

#### 更新内容

1. 新增时光回溯功能
2. 支持安卓APP控制
   1. APP支持导入json格式的自定义命令
   2. APP支持配置(设备ip、自定义命令)本地保存

### 4.0

#### 发布时间 2023/12/03

#### 更新内容

1. APP支持天气密钥配置(不用再改代码编译了)
