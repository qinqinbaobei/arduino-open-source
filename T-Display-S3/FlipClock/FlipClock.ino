#include "frame.h"
#include "TFT_eSPI.h"
#include <WiFi.h>
#include "time.h"
#include <WiFiManager.h>

TFT_eSPI tft = TFT_eSPI();

const char* ntpServer = "ntp.aliyun.com";
const long  gmtOffset_sec =3600*8;           
const int   daylightOffset_sec = 0;
/**背光**/
int back_light = 254;

#define gray 0x6B6D
#define blue 0x0AAD
#define orange 0xC260
#define purple 0x604D
#define green 0x1AE9

char timeHour[3];
char timeMin[3];
char timeSec[3];
char day[3];
char month[6];
char year[5];
char timeWeekDay[3];
String dayInWeek;
String IP;

#define BG TFT_BLACK

#define dnum 11
int data[dnum] = {0,0,0,0,0,0,0,0,0,0,0};
int oldData[dnum] = {0,0,0,0,0,0,0,0,0,0,0};
int xNum[dnum] = {0,50,110,160,220,270,15,65,125,175,255};
int yNum[dnum] = {92,92,92,92,92,92,14,14,14,14,14};

int left=0;
int right=14;

void setup(void)
{    
  pinMode(left,INPUT_PULLUP);
  pinMode(right,INPUT_PULLUP);
  tft.init();
  tft.setRotation(3);
  tft.setSwapBytes(true);
  tft.fillScreen(BG);

  Serial.begin(115200);
  //WiFiManager
  WiFiManager wifiManager;
  wifiManager.autoConnect("Number-Clock");

  Serial.println("");
  Serial.println("WiFi connected.");
  IP=WiFi.localIP().toString();

  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
}

void printLocalTime()
{
  struct tm timeinfo;
  
  if(!getLocalTime(&timeinfo)){
    return;
  }
  
  strftime(timeHour,3, "%H", &timeinfo);
  strftime(timeMin,3, "%M", &timeinfo);
  strftime(timeSec,3, "%S", &timeinfo);

  strftime(timeWeekDay,10, "%A", &timeinfo);
  dayInWeek=String(timeWeekDay);
  
  strftime(day,3, "%d", &timeinfo);
  strftime(month,6, "%B", &timeinfo);
  strftime(year,5, "%Y", &timeinfo);
  
  data[0] = timeinfo.tm_hour/10;
  data[1] = timeinfo.tm_hour%10;
  data[2] = timeinfo.tm_min/10;
  data[3] = timeinfo.tm_min%10;
  data[4] = timeinfo.tm_sec/10;
  data[5] = timeinfo.tm_sec%10;
  data[6] = (timeinfo.tm_mon+1)/10;
  data[7] = (timeinfo.tm_mon+1)%10;
  data[8] = timeinfo.tm_mday/10;
  data[9] = timeinfo.tm_mday%10;
  data[10] = timeinfo.tm_wday;
  if(data[10] == 0){
    data[10] = 7;
  }
}

long t=0;
int xt=230;
int yt=8;

uint32_t volt = (analogRead(4) * 2 * 3.3 * 1000) / 4096;

void showNumber(int32_t x, int32_t y, int num){
    bool background = false;
    int xx,yy;
    for(int i=0;i<64;i++){
        for (int j = 0; j < 50; j++)
        {
          background = false;
          xx = 3;yy=3;
          
          if(-1<j<3 || 46<j<50){
            if(-1<i<3 || 60<i<64){
              if(j<3){
                xx = j;
              }else{
                xx = 49-j;
              }
              if(i<3){
                yy = i;
              }else{
                yy = 63-i;
              }
              if((xx+yy)<3){
                tft.drawPixel(x+j,y+i,BG);
                background = true;
              }
            }
            if(!background){
              tft.drawPixel(x+j,y+i,frame[0][14*64*500+500*i+50*(9-num)+j]);
            }
          }
        }
    }
}

void showGradientNumber(int32_t x, int32_t y, int num, int step){
  bool background = false;
    int xx,yy;
    for(int i=0;i<64;i++){
        for (int j = 0; j < 50; j++)
        {
          background = false;
          xx = 3;yy=3;
          
          if(-1<j<3 || 46<j<50){
            if(-1<i<3 || 60<i<64){
              if(j<3){
                xx = j;
              }else{
                xx = 49-j;
              }
              if(i<3){
                yy = i;
              }else{
                yy = 63-i;
              }
              if((xx+yy)<3){
                tft.drawPixel(x+j,y+i,BG);
                background = true;
              }
            }
            if(!background){
              tft.drawPixel(x+j,y+i,frame[0][(14-step)*64*500+500*i+50*(9-num)+j]);
            }
          }
        }
    }
}

void loop()
{
  if(digitalRead(left)==0){
    if(back_light<100){
      back_light = 100;
    }else{
      back_light -= 10;
    }
    if(back_light>0){
      analogWrite(TFT_BL, back_light);
    }
  }
  
  if(digitalRead(right)==0){
    if(back_light>234){
      back_light = 254;
    }else{
      back_light += 20; 
    }
    analogWrite(TFT_BL, back_light);
  }

  for(int k=0;k<dnum;k++){
    showNumber(xNum[k], yNum[k], data[k]);
    oldData[k] = data[k];
  }
  
  bool dd = false;
  if(t+200<millis()){
    printLocalTime();
    for(int i=0;i<15;i++){
      for(int j=0;j<dnum;j++){
        if(data[j] != oldData[j]){
          dd = true;
          showGradientNumber(xNum[j], yNum[j], oldData[j], i);
        }
      }
      if(dd){
        delay(10);
      }
    }
    t=millis();
  }else{
    delay(16);
  }

}


