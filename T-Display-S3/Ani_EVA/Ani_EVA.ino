#include "run.h"

#include <SPI.h>
#include <TFT_eSPI.h> // Hardware-specific library
TFT_eSPI tft = TFT_eSPI();
int left=0;
int right=14;
int rate=4;

void setup() {
  pinMode(left,INPUT_PULLUP);
  pinMode(right,INPUT_PULLUP);
  // put your setup code here, to run once:
  tft.init();
  tft.setRotation(1);
  tft.setSwapBytes(true);
  tft.fillScreen(TFT_WHITE);
}

void loop() {
  if(digitalRead(left)==0)
  rate--;
  if(digitalRead(right)==0)
  rate++;
  if(rate>10) rate=10;
  if(rate<1) rate=1;
  // put your main code here, to run repeatedly:
  for(int i=0;i<frames;i++)
  {
    delay(10*rate);
    tft.pushImage( 0, 0,animation_width  , animation_height, run[i]);
  }
}