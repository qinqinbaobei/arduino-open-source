#include "GitFans.h"
HTTPClient http;    //创建HTTPClient 实例
String url_bilibiliFans = "";    //资源定位符

long Fansnumber = 0;    //粉丝数

String GitFans::GitURL(String uid)    //拼接资源定位符，获取API链接
{
  url_bilibiliFans =  "http://api.bilibili.com/x/relation/stat?vmid=";
  url_bilibiliFans += uid;

  return url_bilibiliFans;
}

void GitFans::ParseFans(String url)
{
  DynamicJsonDocument doc(1024);  //分配内存
  http.begin(url);    //请求网址

  int httpGet = http.GET();    //获得服务器状态码
  if(httpGet > 0)
  {
    Serial.printf("HTTPGET is %d",httpGet);    //打印服务器状态码

    if(httpGet == HTTP_CODE_OK)    //判断是否请求成功
    {

      String json = http.getString();    //获得响应体信息
      Serial.println(json);    //打印响应体信息

      deserializeJson(doc, json);    //Json解析

      Fansnumber = doc["data"]["follower"];    //获得粉丝数，整形

    }
    else
    {
      Serial.printf("ERROR1!!");
    }
  }
  else
  {
    Serial.printf("ERROR2!!");
  }
  http.end();
}
