# 7段式RGB时钟

## 项目资料

原项目地址：https://www.instructables.com/Lazy-7-Mini/

![](https://content.instructables.com/FN3/2Y9Y/LL3QRFJ0/FN32Y9YLL3QRFJ0.jpg)

## 开发板与接线

当前仓库使用开发板：esp8266


| 作用         | esp8266 | 目标 |
| :------------- | :-------- | ------ |
| 按键A        | D2      |      |
| 按键B        | D1      |      |
| 灯带信号引脚 | D4      | Din  |

参考连线图

![Electronics](https://content.instructables.com/FLF/OBWE/KQZ7HAW3/FLFOBWEKQZ7HAW3.jpg?auto=webp&frame=1&width=1024&height=1024&fit=bounds&md=4e149a8a02e1b26c395e1554e6cefa93)
