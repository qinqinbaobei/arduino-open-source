# ESP32Matrix
ESP32 code to run on a 16x16 WS2812B matrix with Tetris, Snake, Breakout and Bluetooth control via an Android app.
See this youtube video for more information: https://www.youtube.com/watch?v=apmOSQmeKJA


# 说明
## 源代码地址
https://github.com/s-marley/ESP32Matrix
## 相对于原作者代码做了以下变更：
1. Tetris.h  abs(millis() - LastLoop) ->  abs(int(millis() - LastLoop))
2. LEDSprites\LEDSprites.cpp (油管视频有提到)
   1. min -> _min  
   2. max -> _max

## 相关视频地址
https://www.youtube.com/watch?v=cqmWfE1DSyM

https://www.bilibili.com/video/BV1PY4y1x731

## 参考元器件
1. 16x16 ws2812 软屏   50元
2. ESP32 Devkit V1   25元
3. 杜邦线