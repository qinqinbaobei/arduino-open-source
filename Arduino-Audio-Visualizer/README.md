# 基于FFT算法的8x8像素音乐频谱

## 项目来源
https://github.com/mrme88/Arduino-Audio-Visualizer

## 频谱效果
https://www.bilibili.com/video/BV19a411Y7fU

## 使用元件
    1.ESP8266 nodemcu v3
    2.MAX9814 麦克风
    3.8x8 ws2812 灯板