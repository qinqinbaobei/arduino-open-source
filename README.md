# ArduinoOpenSource

#### 介绍

个人Arduino DIY项目

#### 项目目录


| 序号 | 名称                  | 目录                                                                                                                              | 视频                                                            | 进度 |
| ------ | ----------------------- | :---------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------- | ------ |
| 1    | 8x8像素时钟🕒         | [8x8_udp_matrix_time](https://gitee.com/adamhxx/arduino-open-source/tree/master/8x8_udp_matrix_time)                              | [8x8像素时钟](https://www.bilibili.com/video/BV1St4y1H7ZT)      | ✅   |
| 2    | 像素音乐频谱🎵        | [Arduino-Audio-Visualizer](https://gitee.com/adamhxx/arduino-open-source/tree/master/Arduino-Audio-Visualizer)                    | [音乐频谱](https://www.bilibili.com/video/BV1KB4y1y77d)         | ✅   |
| 3    | 像素复古游戏机🎮      | [ESP32Matrix](https://gitee.com/adamhxx/arduino-open-source/tree/master/ESP32Matrix)                                              | [复古游戏机](https://www.bilibili.com/video/BV1fS4y1v7Wt)       | ✅   |
| 4    | 8x32像素时钟🕒        | [MatrixTime_8x32](https://gitee.com/adamhxx/arduino-open-source/tree/master/MatrixTime_8x32)                                      | [像素时钟](https://www.bilibili.com/video/BV1gY411T7hw)         | ✅   |
| 5    | 像素绘图板✏️        | [matrix-draw](https://gitee.com/adamhxx/arduino-open-source/tree/master/matrix-draw)                                              | [像素绘图板](https://www.bilibili.com/video/BV1194y1S7cd)       | ✅   |
| 6    | 合宙ESP32C3音乐频谱🎵 | [ESP32C3-FFT-8x32](https://gitee.com/adamhxx/arduino-open-source/tree/master/Arduino-Audio-Visualizer/ESP32C3-FFT-8x32)           | [合宙版本音乐频谱](https://www.bilibili.com/video/BV18d4y1A7L2) | ✅   |
| 7    | 黑客帝国代码雨        | [matrix-raining-code](https://gitee.com/adamhxx/arduino-open-source/tree/master/matrix-raining-code)                              | [代码雨](https://www.bilibili.com/video/BV18D4y1z7YS)           | ✅   |
| 8    | 合宙ESP32C3像素时钟🕒 | [ESP32C3-CLOCK](https://gitee.com/adamhxx/arduino-open-source/tree/master/ESP32C3-CLOCK)                                          | [合宙版本像素时钟](https://www.bilibili.com/video/BV1Be4y1C7bH) | ✅   |
| 9    | attiny85旋钮          | [Digispark_Knob](https://gitee.com/adamhxx/arduino-open-source/tree/master/KNOB/Digispark_Knob)                                   | [音量旋钮](https://www.bilibili.com/video/BV1rV4y1V7Rf)         | ✅   |
| 10   | XMatrix像素时钟🕒     | [XMatrix](https://gitee.com/adamhxx/arduino-open-source/tree/master/XMatrix)                                                      | [XMatrix](https://www.bilibili.com/video/BV1mt4y1T7Tt)          | ✅   |
| 11   | USB SurfaceDial       | [SurfaceDial/ProMicroVersion](https://gitee.com/adamhxx/arduino-open-source/tree/master/KNOB/SurfaceDial/ProMicroVersion)         | [ProMicroVersion](https://www.bilibili.com/video/BV1rd4y1k7b9)  | ✅   |
| 12   | 合宙c3 SurfaceDial    | [SurfaceDial/SurfaceDial-esp32c3](https://gitee.com/adamhxx/arduino-open-source/tree/master/KNOB/SurfaceDial/SurfaceDial-esp32c3) |                                                                 | ✅   |
| 13   | 7段式RGB时钟          | [7段式RGB时钟](https://gitee.com/adamhxx/arduino-open-source/tree/master/L7M)                                                     |                                                                 | ✅   |
| 14   | T-Display-S3版小电视  | [小电视](https://gitee.com/adamhxx/arduino-open-source/tree/master/T-Display-S3/SmallDesktopDisplay)                              |                                                                 | ✅   |

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
