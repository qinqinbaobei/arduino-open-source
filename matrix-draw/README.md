# 像素绘图板

<img src="./images/draw.jpg" width = "350" height = "180" alt="马里奥示例" align=center />

## 操作说明

    1.单机切换颜色样式(仅时钟模式有效)
    2.双击切换模式
    3.长按调节亮度
    4.三连击保存当前设置(模式/颜色样式/亮度)

## 模式说明

### 一、时钟模式 &#x1F554;

&#x2B50;   默认模式，默认颜色为渐变色，颜色样式修改请查看Gradient(int x, int y)方法

<img src="./images/clock1.jpg" width = "100" height = "120" alt="时钟样式1" align=center />
<img src="./images/clock2.jpg" width = "100" height = "120" alt="时钟样式2" align=center />
<img src="./images/clock3.jpg" width = "100" height = "120" alt="时钟样式3" align=center />

### 二、绘图模式

&#x2B50;   提供两个网页，其一为内置网页

#### 2.1    内置网页 &#x1F604;

&#x2B50;   使用方式：局域网浏览器访问设备ip地址或访问<http://emoji.local/>

<img src="./images/emoji-ex.jpg" width = "504" height = "321" alt="emojis" align=center />

#### 2.2    非内置网页 &#x1F4D1;

&#x2B50;   使用方式：本地打开网页，默认会连上设备，点击右侧图片即可显示对应像素图。也可以在左侧选择颜色进行绘图，点击发送展示。

<img src="./images/draw-ex.jpg" width = "790" height = "425" alt="emoji-ex" align=center />

### 三、动画模式 &#x1F3C3;

&#x2B50;   使用方式：默认为小智皮卡丘像素动画，视频 [&#x25B6;小智、皮卡丘](https://www.bilibili.com/video/BV1Ef4y1o7qX)